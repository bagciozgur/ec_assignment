# Output value definitions

output "lambda_bucket_name" {
  value = aws_s3_bucket.lambda_bucket.id
}

output "function_name" {
  value = aws_lambda_function.embeling_assignment.function_name
}
