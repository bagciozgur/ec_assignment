CREATE SCHEMA IF NOT EXISTS embeling;
CREATE TABLE IF NOT EXISTS "embeling".Book (
    id serial PRIMARY KEY,
    name TEXT NOT NULL,
    author TEXT NOT NULL
);