terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.48.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.2.0"
    }
  }

  required_version = "~> 1.0"
}

provider "aws" {
  region = "eu-central-1"
}

variable "assignment_db_username" {
  default = "ozgur_assignment"
}

variable "assignment_db_password" {
  default = "<hidden>"
}

# Lambda related

resource "random_pet" "lambda_bucket_name" {
  prefix = "embeling-assignment"
  length = 4
}

resource "aws_s3_bucket" "lambda_bucket" {
  bucket = random_pet.lambda_bucket_name.id

  acl           = "private"
  force_destroy = true
}

data "archive_file" "lambda_embeling_assignment" {
  type = "zip"

  source_dir  = "${path.module}/assignment"
  output_path = "${path.module}/assignment.zip"
}

resource "aws_s3_bucket_object" "lambda_embeling_assignment" {
  bucket = aws_s3_bucket.lambda_bucket.id

  key    = "assignment.zip"
  source = data.archive_file.lambda_embeling_assignment.output_path

  etag = filemd5(data.archive_file.lambda_embeling_assignment.output_path)
}

resource "aws_lambda_function" "embeling_assignment" {
  function_name = "EmbelingAssignment"

  s3_bucket = aws_s3_bucket.lambda_bucket.id
  s3_key    = aws_s3_bucket_object.lambda_embeling_assignment.key

  runtime = "nodejs14.x"
  handler = "embeling.handler"

  source_code_hash = data.archive_file.lambda_embeling_assignment.output_base64sha256

  role = aws_iam_role.lambda_exec.arn

  environment {
    variables = {
      DB_INSTANCE_ADDRESS  = aws_db_instance.embeling_db.address
      DB_INSTANCE_USER     = var.assignment_db_username
      DB_INSTANCE_PASSWORD = var.assignment_db_password
    }
  }
}

resource "aws_cloudwatch_log_group" "embeling_assignment" {
  name = "/aws/lambda/${aws_lambda_function.embeling_assignment.function_name}"

  retention_in_days = 30
}

resource "aws_iam_role" "lambda_exec" {
  name = "serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

# API Gateway related

resource "aws_apigatewayv2_api" "lambda" {
  name          = "serverless_lambda_gw"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "lambda" {
  api_id = aws_apigatewayv2_api.lambda.id

  name        = "serverless_lambda_stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "embeling_assignment" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.embeling_assignment.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "embeling_assignment_books" {
  api_id = aws_apigatewayv2_api.lambda.id

  route_key = "ANY /books"
  target    = "integrations/${aws_apigatewayv2_integration.embeling_assignment.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.embeling_assignment.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.lambda.execution_arn}/*/*"
}

# DB related

resource "aws_db_instance" "embeling_db" {
  allocated_storage       = 5
  backup_retention_period = 0
  engine                  = "postgres"
  engine_version          = "13.5"
  name                    = "embeling"
  instance_class          = "db.t3.micro"
  multi_az                = false
  username                = var.assignment_db_username
  password                = var.assignment_db_password
  port                    = 5432
  publicly_accessible     = true
  storage_encrypted       = false
  storage_type            = "gp2"
}

resource "null_resource" "db_setup" {

  depends_on = [aws_db_instance.embeling_db]

  provisioner "local-exec" {
    command = "psql -h \"${aws_db_instance.embeling_db.address}\" -p 5432 -U \"${var.assignment_db_username}\" -d embeling -f \"./init_db.sql\""

    environment = {
      PGPASSWORD = "${var.assignment_db_password}"
    }
  }
}
