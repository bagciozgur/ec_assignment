const Knex = require('knex')
const {
  Model,
} = require('objection')

const knexConfig = {
  client: "pg",
  connection: {
    host: process.env.DB_INSTANCE_ADDRESS,
    port: 5432,
    database: 'embeling',
    user: process.env.DB_INSTANCE_USER,
    password: process.env.DB_INSTANCE_PASSWORD
  }
}

const knex = Knex(knexConfig)
Model.knex(knex)

class Book extends Model {
  static get tableName() {
      return 'embeling.Book'
  }

  static get jsonSchema() {
      return {
          type: 'object',
          required: ['name', 'author'],
          properties: {
              id: { type: 'integer' },
              name: { type: 'string' },
              authorName: { type: 'string' }
          }
      }
  }
}


const getHandler = (event) => {
  const authorQuery = event.queryStringParameters ? event.queryStringParameters['author'] : undefined

  const query = Book.query()

  query.select('*')

  if (authorQuery)
      query.where('author', authorQuery)

  return query.then(books => {
      return {
          statusCode: 200,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(books),
      }
  }).catch(err => {
      return {
          statusCode: 500,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: err
          }),
      }
  })
}

const postHandler = (event) => {
  const reqBody = JSON.parse(event.body)
  delete reqBody.id

  const query = Book.query()

  return query.insert({
      name: reqBody.name,
      author: reqBody.author
  }).then(() => {
      return {
          statusCode: 204
      }
  }).catch(err => {
      return {
          statusCode: 500,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: err
          }),
      }
  })
}

const putHandler = (event) => {
  const bookId = event.queryStringParameters ? event.queryStringParameters['id'] : undefined

  if (!bookId)
      return {
          statusCode: 400,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: 'Book ID must be provided as URL query parameter.'
          }),
      }

  const reqBody = JSON.parse(event.body)
  delete reqBody.id

  const query = Book.query()

  query.findById(bookId)

  return query.patch(reqBody).then(() => {
      return {
          statusCode: 201
      }
  }).catch(err => {
      return {
          statusCode: 500,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: err
          }),
      }
  })
}

const deleteHandler = (event) => {
  const bookId = event.queryStringParameters ? event.queryStringParameters['id'] : undefined

  if (!bookId)
      return {
          statusCode: 400,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: 'Book ID must be provided as URL query parameter.'
          }),
      }

  const query = Book.query()

  query.deleteById(bookId)

  return query.then(() => {
      return {
          statusCode: 204
      }
  }).catch(err => {
      return {
          statusCode: 500,
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              error: err
          }),
      }
  })
}

module.exports.handler = async (event) => {
  const method = event.httpMethod
  if (method == 'GET')
    return getHandler(event)
  else if (method == 'POST')
    return postHandler(event)
  else if (method == 'PUT')
    return putHandler(event)
  else if (method == 'DELETE')
    return deleteHandler(event)
  else
    return {
      statusCode: 501
    }
}
